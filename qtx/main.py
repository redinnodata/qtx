import os
import io
import re

def find_all_files_in_directory(directory):
    for name in os.listdir(directory):
        fname = os.path.join(directory, name)

        if os.path.isdir(fname) and name[0] != '.':
            yield from find_all_files_in_directory(fname)

        elif os.path.isfile(fname):
            yield fname

class SubOnce:
    def __init__(self, substitution):
        self.done = False
        self._substitution = substitution

    def __call__(self, mtc):
        if self.done:
            return ''
        else:
            self.done = True
            return self._substitution

def process(fname):

    with io.open(fname, 'r', encoding='utf-8') as f:
        text = f.read()

    sub = SubOnce('import qtx\n')
    text = re.sub(r'^from\s+PySide\s+import.+\n', sub, text, flags=re.MULTILINE)
    if not sub.done:
        return

    text = re.sub(r'(QtGui|QtCore|QtWebKit|Qt|QtWidgets|QtWebEngineWidgets|QtWebChannel)\.(\w+)', r'qtx.\2', text)
    with io.open(fname, 'w', encoding='utf-8') as f:
        f.write(text)

    #for mtc in re.finditer(r'(QtGui|QtCore|QtWebKit|Qt|QtWebWidgets|QtWebEngine)\.(\w+)', text):
    #    yield mtc.group(2)

    for mtc in re.finditer(r'^from\s+PySide\s+import.+\n', text, flags=re.MULTILINE):
        print( mtc.group().strip())
    #for line in text.split('\n'):
    #    if 'PySide' in line and 'import' in line:
    #        yield line.strip()

if __name__ == '__main__':

    import argparse

    parser = argparse.ArgumentParser(description='Automatically converts PySide code to qtx')

    parser.add_argument('input', nargs='+', help='Input file or directory. If directory is specified, all Python files under this directory will be processed')

    args = parser.parse_args()

    def all_files():
        for name in args.input:
            if os.path.isdir(name):
                yield from (x for x  in find_all_files_in_directory(name) if x.endswith('.py'))
            elif os.path.isfile(name):
                yield name
            else:
                raise RuntimeError('file or directory does not exist: ' + name)

    for fname in all_files():
        print('Processing file', fname)

        process(fname)
