__version__      = '0.0.1'
__description__  = 'Helper to ease switch between PySide and PyQt'
__author__       = 'Mike Kroutikov'
__author_email__ = 'mkroutikov@innodata.com'
__url__          = 'https://bitbucket.org/redinnodata/qtx'
__keywords__     = 'qt pyside pyqt pyqt5'

from . bindings import *
