'''
Created on May 28, 2016

@author: mkroutikov
'''
from setuptools import setup
import os
from qtx import __version__, __description__, __url__, __author__, \
    __author_email__, __keywords__

NAME = 'qtx'

setup(
    name             = NAME,
    version          = __version__,
    description      = __description__,
    long_description = 'See ' + __url__,
    url              = __url__,
    author           = __author__,
    author_email     = __author_email__,
    keywords         = __keywords__,

    license          = 'MIT',
    classifiers = [
        # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
        'Development Status :: 5 - Stable',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: Implementation :: CPython',
        'Programming Language :: Python :: Implementation :: PyPy',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],

    packages=[NAME]
)
