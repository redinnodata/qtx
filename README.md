# qtx

Helper to abstract the differences between PySide and PyQt and PyQt5.

Inspired by [Package `qtpy`](https://github.com/spyder-ide/qtpy), but solves
slightly more specialized problem - conversion from PySide.

It does three distinct things:

1. provides glue layer `qtx` to replace `QtGui`, `QtCore`, `Qt` and other
   PySide/PyQt packages

2. flattens namespace. Instead of looking up documentation and finding out
   sub-package for a particular class or function, just use `qtx` package as
   everything is there.

3. provides a command-line utility to do bulk automatic conversion from PySide
   to qtx.

## Usage

What used to be coded as:
```
from PySide import QtGui

widget = QtGui.QWidget()
```

Now should be coded as:
```
import qtx

widget = qtx.QWidget()
```

## Bulk conversion
```
python -m qtx.main <file1> <file2> <dir1> <dir2>
```
Automatically transforms all files in the list of parameters.

If there is a directory, it will be recursively traversed and
all files matching `*.py` name pattern will be processed.
